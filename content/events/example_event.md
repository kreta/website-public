title: Example Event
short_title: Example
starts: 2017-01-01 12:30
ends: 2017-01-01 20:00
location: AZ Conni
location_link: http://www.azconni.de
location_gps: 51.07686, 13.75107
summary: Short description of the event

Full description of the event.  
**Markdown** *syntax* [supported](http://markdown.de)

