Title: Impressum
slug: impressum
status: hidden

This page is treated differently by the `kreta`-theme. A link to it is placed inside the footer.
