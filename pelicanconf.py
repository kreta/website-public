#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

SITENAME = 'KRETA Dresden'
SITEURL = 'http://localhost:8000'

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'de'

PATH = 'content'
ARTICLE_PATHS = ['news']
PLUGIN_PATHS = ['plugins']

PLUGINS = ['events']
# Events
EVENT_DIR="events"
EVENT_URL="program/{slug}.html"
EVENT_SAVE_AS = 'program/{slug}.html'
EVENT_OVERVIEW_URL = "program.html"
EVENT_EXCLUDES=''
EVENT_ICAL_URL = 'kreta.ics'
EVENT_ICAL_TITLE = 'Kreta-Kalender'


DEFAULT_DATE = 'fs'

THEME = 'themes/kreta'
THEME_KRETA = {
	'no_articles': "Es gibt noch keine News…",
	'no_events': "Es sind noch keine Veranstaltungen bekannt…",
	'ical_link': "Kalenderdatei (iCal)"
}

THEME_STATIC_DIR = 'theme'

PAGE_ORDER_BY = 'attribute'

# Include old website and downloads folder
STATIC_PATHS = ['legacy','downloads']

STATIC_EXCLUDE_SOURCES = True

# Clean output directory
DELETE_OUTPUT_DIRECTORY = True

# Remove all unused datatypes
CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
ARCHIVE_SAVE_AS = ''
ARCHIVES_SAVE_AS = ''
TAG_SAVE_AS = ''
TAGS_SAVE_AS = ''

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
