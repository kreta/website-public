from pelican.contents import Page

class Event(Page):
    """
    Define all properties that create an event
    """
    base_properties = ('starts','ends','title','short_title','location','location_link')
    mandatory_properties = ('starts','ends','title','location')
    default_template = 'event'
