#!/usr/bin/env python

from __future__ import print_function

from pelican import signals as core_signals
from .generators import EventsGenerator


def initEvents(generator):
	events_dict = {}
	try:
		events = generator.context['events']
	except KeyError:
		generator.context['events'] = events = {}

def get_generators(generators):
	return EventsGenerator

def register():
	core_signals.get_generators.connect(get_generators)
